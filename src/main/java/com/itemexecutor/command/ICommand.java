package com.itemexecutor.command;

import org.bukkit.command.CommandSender;

import java.util.List;

/**
 * Created by derek on 11/16/2015.
 */
public interface ICommand {

    String getName();

    String[] getAliases();

    String getPermission();

    String getUsageString(String label, CommandSender sender);

    String getDescription();

    boolean canBeConsole();

    boolean canBeCommandBlock();

    boolean onCommand(CommandSender sender, String label, String... args);

    List<String> onTabComplete(CommandSender sender, String label, String... args);
}