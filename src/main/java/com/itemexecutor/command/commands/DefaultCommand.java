package com.itemexecutor.command.commands;

import com.itemexecutor.ItemExecutor;
import com.itemexecutor.command.ICommand;
import org.bukkit.command.CommandSender;

import java.util.List;
import java.util.TreeMap;

import static com.itemexecutor.util.ChatUtil.t;

/**
 * Created by derek on 11/16/2015.
 */
public class DefaultCommand implements ICommand {

    @Override
    public String getName() {
        return "";
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getPermission() {
        return "itemexecutor";
    }

    @Override
    public String getUsageString(String label, CommandSender sender) {
        return label;
    }

    @Override
    public String getDescription() {
        return "Lists all item executor commands that you have permission to.";
    }

    @Override
    public boolean canBeConsole() {
        return true;
    }

    @Override
    public boolean canBeCommandBlock() {
        return false;
    }

    @Override
    public boolean onCommand(CommandSender sender, String label, String... args) {
        if (args.length != 0) {
            return false;
        }

        sender.sendMessage(t("helpTitle"));
        TreeMap<String, ICommand> commands = new TreeMap<>(ItemExecutor.getInstance().getCommandManager().getCommandDispatcher().getCommands(sender));
        for (String commandName : commands.keySet()) {
            ICommand command = commands.get(commandName);

            if (commandName.equals("")) {
                sender.sendMessage(t("helpCommand", "itemexecutor" + commandName, command.getDescription()));
            } else {
                sender.sendMessage(t("helpCommand", "itemexecutor " + commandName, command.getDescription()));
            }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        return null;
    }
}
