package com.itemexecutor.command.commands;

import com.itemexecutor.ItemExecutor;
import com.itemexecutor.command.ICommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Item;

import java.util.List;

import static com.itemexecutor.util.ChatUtil.t;

/**
 * Created by derek on 11/18/2015.
 */
public class ReloadCommand implements ICommand {

    @Override
    public String getName() {
        return "reload";
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getPermission() {
        return "itemexecutor.reload";
    }

    @Override
    public String getUsageString(String label, CommandSender sender) {
        return label;
    }

    @Override
    public String getDescription() {
        return "Reload the config from disk.";
    }

    @Override
    public boolean canBeConsole() {
        return true;
    }

    @Override
    public boolean canBeCommandBlock() {
        return false;
    }

    @Override
    public boolean onCommand(CommandSender sender, String label, String... args) {
        if (args.length != 0) {
            return false;
        }

        ItemExecutor.getInstance().reload();
        sender.sendMessage(t("reloadComplete", "ItemExecutor", ItemExecutor.getInstance().getConfigOptions().getVersion()));
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        return null;
    }
}

