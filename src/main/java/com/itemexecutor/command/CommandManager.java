package com.itemexecutor.command;

import com.itemexecutor.ItemExecutor;
import com.itemexecutor.command.commands.DefaultCommand;
import com.itemexecutor.command.commands.ReloadCommand;
import org.bukkit.command.PluginCommand;

/**
 * Created by derek on 11/16/2015.
 */
public class CommandManager {

    private final CommandDispatcher commandDispatcher;

    public CommandManager(ItemExecutor plugin) {
        PluginCommand pluginCommand = plugin.getCommand("itemexecutor");
        commandDispatcher = new CommandDispatcher("itemexecutor", "ItemExecutor main plugin command.");

        commandDispatcher.setDefault(new DefaultCommand());
        commandDispatcher.registerCommand(new ReloadCommand());

        pluginCommand.setExecutor(commandDispatcher);
        pluginCommand.setTabCompleter(commandDispatcher);
    }

    public CommandDispatcher getCommandDispatcher() {
        return commandDispatcher;
    }
}
