package com.itemexecutor.command;

import org.bukkit.ChatColor;
import org.bukkit.command.*;

import java.util.*;

import static com.itemexecutor.util.ChatUtil.t;

/**
 * Created by derek on 11/16/2015.
 */
public class CommandDispatcher implements CommandExecutor, TabCompleter {

    private final String commandName;
    private final String commandDescription;
    private final Map<String, ICommand> subCommands;
    private ICommand command;

    public CommandDispatcher(String commandName, String description) {
        this.commandName = commandName;
        this.commandDescription = description;
        subCommands = new HashMap<>();
    }


    public void registerCommand(ICommand command) {
        subCommands.put(command.getName(), command);
    }

    public void setDefault(ICommand command) {
        this.command = command;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0 && this.command == null) {
            displayUsage(sender, label, null);
            return true;
        }

        ICommand com = null;
        String subCommand = "";

        String[] subArgs = args;

        if (args.length > 0) {
            subCommand = args[0].toLowerCase();
            subArgs = (args.length > 1 ? Arrays.copyOfRange(args, 1, args.length) : new String[0]);

            if (subCommands.containsKey(subCommand))
                com = subCommands.get(subCommand);
            else {
                AliasCheck:
                for (Map.Entry<String, ICommand> ent : subCommands.entrySet()) {
                    if (ent.getValue().getAliases() != null) {
                        String[] aliases = ent.getValue().getAliases();
                        for (String alias : aliases) {
                            if (subCommand.equalsIgnoreCase(alias)) {
                                com = ent.getValue();
                                break AliasCheck;
                            }
                        }
                    }
                }
            }
        }

        if (com == null)
            com = this.command;

        if (com == null) {
            displayUsage(sender, label, subCommand);
            return true;
        }

        if (!com.canBeConsole() && (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender)) {
            if (com == this.command)
                displayUsage(sender, label, subCommand);
            else
                sender.sendMessage(ChatColor.RED + "/" + label + " " + subCommand + " cannot be called from the console.");
            return true;
        }
        if (!com.canBeCommandBlock() && sender instanceof BlockCommandSender) {
            if (com == this.command)
                displayUsage(sender, label, subCommand);
            else
                sender.sendMessage(ChatColor.RED + "/" + label + " " + subCommand + " cannot be called from a command block.");
            return true;
        }
        if (com.getPermission() != null && !sender.hasPermission(com.getPermission())) {
            StringBuilder stringBuilder = new StringBuilder(label);
            stringBuilder.append(" ");
            stringBuilder.append(subCommand);
            for (String string : subArgs) {
                stringBuilder.append(" ");
                stringBuilder.append(string);
            }
            sender.sendMessage(t("noPermission", com.getPermission(), stringBuilder.toString()));
            return true;
        }

        if (!com.onCommand(sender, subCommand, subArgs)) {
            sender.sendMessage(t("incorrectFormat", "/" + commandName + " " + com.getUsageString(subCommand, sender)));
        }

        return true;
    }

    private void displayUsage(CommandSender sender, String label, String subCommand) {
        String usage = "";

        boolean first = true;
        boolean odd = true;
        for (ICommand command : subCommands.values()) {
            if (!command.canBeConsole() && (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender))
                continue;
            if (command.getPermission() != null && !sender.hasPermission(command.getPermission()))
                continue;
            if (odd)
                usage += ChatColor.WHITE;
            else
                usage += ChatColor.GRAY;
            odd = !odd;

            if (first)
                usage += command.getName();
            else
                usage += ", " + command.getName();

            first = false;
        }

        if (subCommand != null)
            sender.sendMessage(ChatColor.RED + "Unknown command: " + ChatColor.RESET + "/" + label + " " + ChatColor.GOLD + subCommand);
        else
            sender.sendMessage(ChatColor.RED + "No command specified: " + ChatColor.RESET + "/" + label + ChatColor.GOLD + " <command>");

        if (!first) {
            sender.sendMessage("Valid commands are:");
            sender.sendMessage(usage);
        } else
            sender.sendMessage("There are no commands available to you");


    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> results = new ArrayList<>();
        if (args.length == 1) {
            for (ICommand registeredCommand : subCommands.values()) {
                if (registeredCommand.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
                    if (!registeredCommand.canBeConsole() && (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender))
                        continue;
                    if (registeredCommand.getPermission() != null && !sender.hasPermission(registeredCommand.getPermission()))
                        continue;

                    results.add(registeredCommand.getName());
                }
            }
        } else {
            String subCommand = args[0].toLowerCase();
            String[] subArgs = (args.length > 1 ? Arrays.copyOfRange(args, 1, args.length) : new String[0]);

            ICommand com = null;
            if (subCommands.containsKey(subCommand)) {
                com = subCommands.get(subCommand);
            } else {
                AliasCheck:
                for (Map.Entry<String, ICommand> ent : subCommands.entrySet()) {
                    if (ent.getValue().getAliases() != null) {
                        String[] aliases = ent.getValue().getAliases();
                        for (String alias : aliases) {
                            if (subCommand.equalsIgnoreCase(alias)) {
                                com = ent.getValue();
                                break AliasCheck;
                            }
                        }
                    }
                }
            }

            if (com == null) {
                return results;
            }
            if (!com.canBeConsole() && (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender)) {
                return results;
            }
            if (com.getPermission() != null && !sender.hasPermission(com.getPermission())) {
                return results;
            }

            results = com.onTabComplete(sender, subCommand, subArgs);
            if (results == null)
                return new ArrayList<>();
        }
        return results;
    }

    public String getCommandHelp() {
        return commandName + commandDescription;
    }

    public Map<String, ICommand> getCommands(CommandSender commandSender) {
        Map<String, ICommand> commands = new HashMap<>();

        commands.putAll(subCommands);
        commands.put("", command);
        List<String> toRemove = new ArrayList<>();
        for (String name : commands.keySet()) {
            ICommand iCommand = commands.get(name);
            if (iCommand.getPermission() != null) {
                if (!commandSender.hasPermission(iCommand.getPermission())) {
                    toRemove.add(name);
                }
            }
        }
        for (String string : toRemove) {
            commands.remove(string);
        }
        return commands;
    }
}