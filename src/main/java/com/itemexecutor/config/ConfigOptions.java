package com.itemexecutor.config;

import com.itemexecutor.ItemExecutor;
import com.itemexecutor.item.ExecutorItem;
import com.itemexecutor.item.ItemManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by derek on 11/16/2015.
 */
public class ConfigOptions {

    private final ItemExecutor plugin;

    private FileConfiguration fileConfiguration;

    private String world;
    private String version;

    public ConfigOptions(ItemExecutor plugin, FileConfiguration fileConfiguration) {
        this.plugin = plugin;
        this.fileConfiguration = fileConfiguration;

        if (fileConfiguration == null) {
            this.fileConfiguration = plugin.getConfig();
        }

        loadConfig();
        loadInventoryConfig();
    }

    private void loadConfig() {
        if (!new File(plugin.getDataFolder(), "config.yml").exists()) {
            fileConfiguration.options().copyDefaults(true);
            plugin.saveConfig();
        }

        FileConfiguration updatedConfiguration = new ConfigUpdater(fileConfiguration).update();

        if (updatedConfiguration != null) {
            fileConfiguration = updatedConfiguration;
            plugin.saveConfig();
        }

        world = fileConfiguration.getString(ConfigPaths.WORLD);
        version = fileConfiguration.getString(ConfigPaths.VERSION);
    }

    private void loadInventoryConfig() {
        ItemManager itemManager = plugin.getItemManager();
        ConfigurationSection inventorySection = fileConfiguration.getConfigurationSection(ConfigPaths.INVENTORY);
        ConfigurationSection slotSection = inventorySection.getConfigurationSection(ConfigPaths.SLOTS);

        itemManager.setRemoveItemsNotInList(inventorySection.getBoolean(ConfigPaths.REMOVE_ITEMS_NOT_IN_LIST));

        for (String stringSlot : slotSection.getKeys(false)) {
            ConfigurationSection itemSection = inventorySection.getConfigurationSection(ConfigPaths.ITEMS);

            int slot = Integer.valueOf(stringSlot);
            String itemName = slotSection.getString(stringSlot);
            ItemStack itemStack;
            Map<String, Boolean> commandMap = new HashMap<>();
            boolean droppable;
            boolean forceSlot;

            if (itemSection.contains(itemName)) {
                ConfigurationSection currentItemSection = itemSection.getConfigurationSection(itemName);
                ConfigurationSection commandSection = currentItemSection.getConfigurationSection(ConfigPaths.COMMANDS);

                for (String index : commandSection.getKeys(false)) {
                    ConfigurationSection currentCommandSection = commandSection.getConfigurationSection(index);

                    commandMap.put(currentCommandSection.getString(ConfigPaths.COMMAND), currentCommandSection.getBoolean(ConfigPaths.CONSOLE_SENDER));
                }
                droppable = currentItemSection.getBoolean(ConfigPaths.DROPPABLE);
                forceSlot = currentItemSection.getBoolean(ConfigPaths.FORCE_SLOT);
                itemStack = loadItem(currentItemSection);

                itemManager.setExecutorItem(slot, new ExecutorItem(itemStack, commandMap, droppable, forceSlot));
            }
        }
    }

    private ItemStack loadItem(ConfigurationSection configurationSection) {
        ItemStack itemStack;
        ConfigurationSection itemConfigurationSection = configurationSection.getConfigurationSection(ConfigPaths.ITEM_STACK);

        int amount = itemConfigurationSection.getInt(ConfigPaths.AMOUNT);
        String displayName = itemConfigurationSection.getString(ConfigPaths.DISPLAY_NAME).replace("&", ChatColor.COLOR_CHAR + "");
        List<String> lore = itemConfigurationSection.getStringList(ConfigPaths.LORE);
        List<String> formattedLore = new ArrayList<>();
        Material material = Material.valueOf(itemConfigurationSection.getString(ConfigPaths.MATERIAL));
        byte metaData = Byte.valueOf(itemConfigurationSection.getString(ConfigPaths.META_DATA));

        itemStack = new ItemStack(material, amount, metaData);
        ItemMeta itemMeta = itemStack.getItemMeta();

        if (!displayName.equals("")) {
            itemMeta.setDisplayName(displayName);
        }
        if (!lore.isEmpty()) {
            for (String currentLore : lore) {
                formattedLore.add(currentLore.replaceAll("&", "\247"));
            }
            itemMeta.setLore(formattedLore);
        }

        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public String getWorld() {
        return world;
    }

    public String getVersion() {
        return version;
    }
}
