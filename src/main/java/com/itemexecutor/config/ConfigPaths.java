package com.itemexecutor.config;

/**
 * Created by derek on 11/16/2015.
 */
public class ConfigPaths {
    public static final String INVENTORY = "inventory";

    public static final String SLOTS = "slots";
    public static final String ITEMS = "items";

    public static final String COMMANDS = "commands";

    public static final String COMMAND = "command";
    public static final String CONSOLE_SENDER = "consoleSender";

    public static final String DROPPABLE = "droppable";
    public static final String FORCE_SLOT = "forceSlot";
    public static final String ITEM_STACK = "itemStack";

    public static final String AMOUNT = "amount";
    public static final String DISPLAY_NAME = "displayName";
    public static final String LORE = "lore";
    public static final String MATERIAL = "material";
    public static final String META_DATA = "metaData";

    public static final String REMOVE_ITEMS_NOT_IN_LIST = "removeItemsNotInList";

    public static final String WORLD = "world";
    public static final String VERSION = "version";
}
