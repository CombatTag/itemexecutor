package com.itemexecutor.config.version;

import com.itemexecutor.config.ConfigPaths;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by derek on 11/16/2015.
 */
public abstract class Config0_1 {

    public static boolean equals(FileConfiguration fileConfiguration) {
        if (fileConfiguration.contains(ConfigPaths.VERSION)) {
            if (fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1")) {
                return true;
            }
        }
        return false;
    }
}
