package com.itemexecutor;

import com.itemexecutor.command.CommandManager;
import com.itemexecutor.config.ConfigOptions;
import com.itemexecutor.item.ItemManager;
import com.itemexecutor.listener.InventoryListener;
import com.itemexecutor.listener.PlayerListener;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

/**
 * Created by derek on 11/16/2015.
 */
public class ItemExecutor extends JavaPlugin {

    private static ItemExecutor instance;

    private CommandManager commandManager;
    private ConfigOptions configOptions;
    private ItemManager itemManager;
    private FileConfiguration messages;

    @Override
    public void onEnable() {
        instance = this;

        itemManager = new ItemManager(this);
        configOptions = new ConfigOptions(this, getConfig());
        commandManager = new CommandManager(this);

        registerListeners(getServer().getPluginManager());

        loadMessages();

        for (Player player : getServer().getOnlinePlayers()) {
            itemManager.updateInventory(player);
        }
    }

    public void reload() {
        itemManager = new ItemManager(this);
        configOptions = new ConfigOptions(this, YamlConfiguration.loadConfiguration(new File(ItemExecutor.getInstance().getDataFolder(), "config.yml")));
        for (Player player : getServer().getOnlinePlayers()) {
            itemManager.updateInventory(player);
        }
    }

    private void registerListeners(PluginManager pluginManager) {
        HandlerList.unregisterAll(this);

        final InventoryListener inventoryListener = new InventoryListener(this);
        final PlayerListener playerListener = new PlayerListener(this);

        pluginManager.registerEvents(inventoryListener, this);
        pluginManager.registerEvents(playerListener, this);
    }

    private void loadMessages() {
        try {
            Reader messagesStream = new InputStreamReader(this.getResource("messages.yml"), "UTF8");
            messages = YamlConfiguration.loadConfiguration(messagesStream);
        } catch (UnsupportedEncodingException e) {
            System.out.println("An error has occurred while loading the plugin messages.");
        }
    }

    public static ItemExecutor getInstance() {
        return instance;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public ConfigOptions getConfigOptions() {
        return configOptions;
    }

    public ItemManager getItemManager() {
        return itemManager;
    }

    public FileConfiguration getMessages() {
        return messages;
    }
}
