package com.itemexecutor.listener;

import com.itemexecutor.ItemExecutor;
import com.itemexecutor.item.ExecutorItem;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;

/**
 * Created by derek on 11/18/2015.
 */
public class InventoryListener implements Listener {

    private ItemExecutor plugin;

    public InventoryListener(ItemExecutor plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        ExecutorItem executorItem = plugin.getItemManager().getItem(event.getOldCursor());

        if (executorItem != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        ExecutorItem executorItem = plugin.getItemManager().getItem(event.getCurrentItem());

        if (executorItem == null) {
            if (event.getAction() == InventoryAction.HOTBAR_SWAP || event.getAction() == InventoryAction.HOTBAR_MOVE_AND_READD) {
                executorItem = plugin.getItemManager().getItem(event.getWhoClicked().getInventory().getItem(event.getHotbarButton()));
            }
        }

        if (executorItem != null) {
            if (executorItem.isForceSlot()) {
                event.setCancelled(true);
            } else if (event.getView().getTopInventory() != null) {
                switch (event.getAction()) {
                    case HOTBAR_SWAP:
                         event.setCancelled(true);
                        return;
                    case HOTBAR_MOVE_AND_READD:
                        event.setCancelled(true);
                        return;
                    case MOVE_TO_OTHER_INVENTORY:
                        if (event.getClickedInventory() == event.getView().getBottomInventory()) {
                            event.setCancelled(true);
                            return;
                        }
                    default:
                }
            }
        }
    }
}
