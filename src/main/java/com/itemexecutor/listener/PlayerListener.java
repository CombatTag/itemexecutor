package com.itemexecutor.listener;

import com.itemexecutor.ItemExecutor;
import com.itemexecutor.item.ExecutorItem;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.*;

/**
 * Created by derek on 11/17/2015.
 */
public class PlayerListener implements Listener {

    private ItemExecutor plugin;

    public PlayerListener(ItemExecutor plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() != Action.PHYSICAL) {
            ExecutorItem executorItem = plugin.getItemManager().getItem(event.getPlayer().getItemInHand());

            if (executorItem != null) {
                for (Map.Entry<String, Boolean> entry : executorItem.getCommandMap().entrySet()) {
                    plugin.getServer().dispatchCommand(entry.getValue() ? plugin.getServer().getConsoleSender() : event.getPlayer(), entry.getKey().replace("<PLAYER>", event.getPlayer().getName()));
                }
            }
        }
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        ExecutorItem executorItem = plugin.getItemManager().getItem(event.getItemDrop().getItemStack());
        if (executorItem != null) {
            if (!executorItem.isDroppable()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        plugin.getItemManager().updateInventory(event.getPlayer());
    }

    @EventHandler
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();
        World toWorld = player.getWorld();

        if (toWorld.getName().equalsIgnoreCase(plugin.getConfigOptions().getWorld())) {
            plugin.getItemManager().updateInventory(player);
        }
    }
}
