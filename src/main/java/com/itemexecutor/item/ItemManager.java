package com.itemexecutor.item;

import com.itemexecutor.ItemExecutor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by derek on 11/17/2015.
 */
public class ItemManager {

    private final ItemExecutor plugin;

    private Map<Integer, ExecutorItem> executorItems = new HashMap<>();
    private boolean removeItemsNotInList;

    public ItemManager(ItemExecutor plugin) {
        this.plugin = plugin;
    }

    public void setExecutorItem(int slot, ExecutorItem executorItem) {
        executorItems.put(slot, executorItem);
    }

    public Map<Integer, ExecutorItem> getExecutorItems() {
        return executorItems;
    }

    public boolean isRemoveItemsNotInList() {
        return removeItemsNotInList;
    }

    public void setRemoveItemsNotInList(boolean removeItemsNotInList) {
        this.removeItemsNotInList = removeItemsNotInList;
    }

    public ExecutorItem getItem(ItemStack itemStack) {
        for (ExecutorItem currentExecutorItem : plugin.getItemManager().getExecutorItems().values()) {
            if (currentExecutorItem.getItemStack().equals(itemStack)) {
                return currentExecutorItem;
            }
        }
        return null;
    }

    public void updateInventory(final Player player) {
        Bukkit.getScheduler().runTask(plugin, new Runnable() {
            @Override
            public void run() {
                player.getInventory().setHelmet(null);
                player.getInventory().setChestplate(null);
                player.getInventory().setLeggings(null);
                player.getInventory().setBoots(null);

                if (plugin.getItemManager().isRemoveItemsNotInList()) {
                    for (ItemStack itemStack : player.getInventory().getContents()) {
                        if (plugin.getItemManager().getItem(itemStack) == null) {
                            player.getInventory().remove(itemStack);
                        }
                    }
                }
                for (Map.Entry<Integer, ExecutorItem> entry : plugin.getItemManager().getExecutorItems().entrySet()) {
                    List<Integer> slots = hasItem(player, entry.getValue().getItemStack());

                    if (slots.isEmpty()) {
                        player.getInventory().setItem(entry.getKey(), entry.getValue().getItemStack());
                    } else {
                        if (entry.getValue().isForceSlot()) {
                            for (Integer integer : slots) {
                                if (!integer.equals(entry.getKey())) {
                                    player.getInventory().setItem(integer, null);
                                    player.getInventory().setItem(entry.getKey(), entry.getValue().getItemStack());
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private List<Integer> hasItem(Player player, ItemStack executorItem) {
        List<Integer> slots = new ArrayList<>();

        for (int i = 0; i < player.getInventory().getContents().length; i++) {
            ItemStack itemStack = player.getInventory().getItem(i);
            if (itemStack != null) {
                if (itemStack.equals(executorItem)) {
                    slots.add(i);
                }
            }
        }
        return slots;
    }
}
