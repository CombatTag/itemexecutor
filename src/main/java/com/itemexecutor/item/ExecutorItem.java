package com.itemexecutor.item;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by derek on 11/17/2015.
 */
public class ExecutorItem {

    private Map<String, Boolean> commandMap = new HashMap<>();
    private boolean droppable;
    private boolean forceSlot;
    private ItemStack itemStack;

    public ExecutorItem(ItemStack itemStack, Map<String, Boolean> commandMap, boolean droppable, boolean forceSlot) {
        this.commandMap = commandMap;
        this.droppable = droppable;
        this.forceSlot = forceSlot;
        this.itemStack = itemStack;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public Map<String, Boolean> getCommandMap() {
        return commandMap;
    }

    public void setCommandMap(Map<String, Boolean> commandMap) {
        this.commandMap = commandMap;
    }

    public boolean isDroppable() {
        return droppable;
    }

    public void setDroppable(boolean droppable) {
        this.droppable = droppable;
    }

    public boolean isForceSlot() {
        return forceSlot;
    }

    public void setForceSlot(boolean forceSlot) {
        this.forceSlot = forceSlot;
    }
}
