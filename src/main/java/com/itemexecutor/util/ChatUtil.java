package com.itemexecutor.util;

import com.itemexecutor.ItemExecutor;
import org.bukkit.ChatColor;

/**
 * Created by derek on 11/16/2015.
 */
public class ChatUtil {

    public static String t(final String string, final String... values) {
        String message = ItemExecutor.getInstance().getMessages().getString(string);
        int index = 0;
        while (message.contains("{" + index + "}")) {
            message = message.replace("{" + index + "}", values[index]);
            index++;
        }
        return ChatColor.translateAlternateColorCodes('&', message);
    }
}
